package hr.tvz.nbaFantasyRest;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class NbaFantasyRestApplication {

	private static final Logger logger = LogManager.getLogger(NbaFantasyRestApplication.class);

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(NbaFantasyRestApplication.class, args);
	}
}
