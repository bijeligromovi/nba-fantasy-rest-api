package hr.tvz.nbaFantasyRest.controller.league;

import hr.tvz.nbaFantasyRest.controller.league.dto.LeagueDto;
import hr.tvz.nbaFantasyRest.service.league.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/league")
public class LeagueController {

    @Autowired
    LeagueService leagueService;

    @GetMapping
    List<LeagueDto> getLeagues(){
        return leagueService.getLeagues();
    }

    @GetMapping(value = "/myLeagues")
    List<LeagueDto> getMyLeagues(Principal principal){
        return leagueService.getMyLeagues(principal);
    }

    @PostMapping
    ResponseEntity joinLeague(@RequestBody Long id, Principal principal) {
        try{
            leagueService.joinLeague(id, principal);
            return ResponseEntity.ok().body("{\"message\":\"Team created\"}");
        } catch (Exception e){
            return ResponseEntity.status(500).body(e);
        }
    }

    @PostMapping("/create")
    ResponseEntity createLeague(@RequestBody String name) {
        try{
            //TEST
            leagueService.createLeague(name);
            return ResponseEntity.ok().body("{\"message\":\"League created\"}");
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e);
        }
    }
}
