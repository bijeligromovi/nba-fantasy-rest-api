package hr.tvz.nbaFantasyRest.controller.league.dto;

import lombok.Data;

import java.util.List;

@Data
public class LeagueDto {
    private String name;
    private List<LeaguePlayerDto> players;

}

