package hr.tvz.nbaFantasyRest.controller.league.dto;

import lombok.Data;

@Data
public class LeaguePlayerDto {
    private String name;
    private Integer points;


    public LeaguePlayerDto(String name, Integer points) {
        this.name = name;
        this.points = points;
    }
}
