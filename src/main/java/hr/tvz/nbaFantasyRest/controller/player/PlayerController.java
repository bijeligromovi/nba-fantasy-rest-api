package hr.tvz.nbaFantasyRest.controller.player;

import hr.tvz.nbaFantasyRest.controller.player.dto.DropdownFiltersValuesDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.FiltersDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;
import hr.tvz.nbaFantasyRest.service.player.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @PostMapping(value = "/players/all")
    public List<PlayerDto> getAllPlayers(@RequestBody FiltersDto filtersDto) {
        return playerService.getAllPlayers(filtersDto);
    }

    @GetMapping(value ="/players/getFilters")
    public List<DropdownFiltersValuesDto> getDropdownValues(){
        return playerService.getDropdownValues();
    }
}
