package hr.tvz.nbaFantasyRest.controller.player.dto;

import lombok.Data;

@Data
public class DropdownFiltersValuesDto {

    private String teamId;
    private String teamName;
}
