package hr.tvz.nbaFantasyRest.controller.player.dto;

import lombok.Data;

@Data
public class FiltersDto {
    private String teamId;
    private String playerName;
    private String price;
    private int page;
    private int size;


    public FiltersDto(){}

    public FiltersDto(String teamId, String playerName, String price, int page, int size){

        this.teamId = teamId;
        this.playerName=playerName;
        this.price=price;
        this.page=page;
        this.size=size;
    }

}
