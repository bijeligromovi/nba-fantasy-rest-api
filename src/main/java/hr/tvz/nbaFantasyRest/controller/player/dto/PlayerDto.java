package hr.tvz.nbaFantasyRest.controller.player.dto;

import lombok.Data;

@Data
public class PlayerDto {
    private String id;
    private Long myPlayerId;
    private boolean active;
    private String name;
    private String surname;
    private String teamName;
    private PlayerStatisticsDto statistics;
}
