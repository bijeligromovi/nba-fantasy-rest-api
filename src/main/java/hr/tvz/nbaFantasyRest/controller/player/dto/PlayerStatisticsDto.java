package hr.tvz.nbaFantasyRest.controller.player.dto;

import lombok.Data;

import java.util.Date;

@Data
public class PlayerStatisticsDto {
    private Integer points;
    private Integer rebounds;
    private Integer assists;
    private Integer price;
    private Integer height;
    private Integer weight;
    private Integer jersey;
    private Integer yearsPro;
    private Date dateOfBirth;
}
