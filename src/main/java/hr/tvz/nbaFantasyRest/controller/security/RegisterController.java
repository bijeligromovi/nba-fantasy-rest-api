package hr.tvz.nbaFantasyRest.controller.security;

import hr.tvz.nbaFantasyRest.controller.security.dto.UserDto;
import hr.tvz.nbaFantasyRest.service.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@CrossOrigin(allowCredentials = "true")
public class RegisterController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/register")
    public ResponseEntity register(@RequestBody UserDto user) {
        if (user != null) {
            try {
                userService.registerUser(user);
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
            }
        }
        return ResponseEntity.ok("User created!");
    }
}
