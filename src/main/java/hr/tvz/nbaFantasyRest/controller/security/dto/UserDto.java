package hr.tvz.nbaFantasyRest.controller.security.dto;

import lombok.Data;

@Data
public class UserDto {

    public String userName;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public String confirmPassword;
}
