package hr.tvz.nbaFantasyRest.controller.team;

import hr.tvz.nbaFantasyRest.controller.team.dto.MyTeamDto;
import hr.tvz.nbaFantasyRest.controller.team.dto.SaveTeamDto;
import hr.tvz.nbaFantasyRest.service.team.TeamService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/team")
@CrossOrigin(allowCredentials = "true")
@Log4j
public class TeamController {

    @Autowired
    TeamService teamService;

    @PostMapping(value = "/create")
    public ResponseEntity addPlayer(@RequestBody SaveTeamDto saveTeamDto, Principal principal) {
        try{
            teamService.addToTeam(principal, saveTeamDto);
            return ResponseEntity.ok().body("{\"message\":\"Team created\"}");
        }catch (Exception e) {
            log.error(e);
            return ResponseEntity.status(500).body(e);
        }
    }

    @GetMapping
    public MyTeamDto getTeam(Principal principal) {
        return teamService.getTeam(principal);
    }

    @GetMapping(value = "/exists")
    public Boolean teamExists(Principal principal){
        return teamService.teamExsists(principal);
    }

}
