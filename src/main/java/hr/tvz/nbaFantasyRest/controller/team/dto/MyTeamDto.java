package hr.tvz.nbaFantasyRest.controller.team.dto;

import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;
import lombok.Data;

import java.util.List;

@Data
public class MyTeamDto {
    private String name;
    private int points;
    private List<PlayerDto> onCourt;
    private List<PlayerDto> onBench;
}
