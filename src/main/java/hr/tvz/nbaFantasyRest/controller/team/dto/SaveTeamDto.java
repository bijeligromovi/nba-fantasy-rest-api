package hr.tvz.nbaFantasyRest.controller.team.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SaveTeamDto {
    private String name;
    private List<String> onCourt;
    private List<String> substitutions;
}
