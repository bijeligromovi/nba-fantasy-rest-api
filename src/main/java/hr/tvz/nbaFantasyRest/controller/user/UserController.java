package hr.tvz.nbaFantasyRest.controller.user;

import hr.tvz.nbaFantasyRest.controller.user.dto.UserDto;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.repository.UserRepository;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/user")
@CrossOrigin(allowCredentials = "true")
@Log4j
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping(value="/details")
    public UserDto getUser(Principal principal) {

        UserDto userDetails = new UserDto();

        ApplicationUser appUser = userRepository.findByUserName(principal.getName());

        userDetails.setFirstname(appUser.getFirstName());
        userDetails.setLastname(appUser.getLastName());
        userDetails.setTeamname(appUser.getMyTeam().getName());

        return userDetails;
    }


}

