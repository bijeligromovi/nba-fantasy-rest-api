package hr.tvz.nbaFantasyRest.controller.user.dto;

import lombok.Data;

@Data
public class UserDto {

    String firstname;
    String lastname;
    String teamname;
}
