package hr.tvz.nbaFantasyRest.mappers;

import hr.tvz.nbaFantasyRest.controller.league.dto.LeagueDto;
import hr.tvz.nbaFantasyRest.controller.league.dto.LeaguePlayerDto;
import hr.tvz.nbaFantasyRest.model.League;

import java.util.stream.Collectors;

public class LeagueMapper {

    public LeagueDto entityToDto(League league){
        LeagueDto leagueDto = new LeagueDto();
        leagueDto.setName(league.getName());
        leagueDto.setPlayers(league.getTeams().stream()
                .map(myTeam -> new LeaguePlayerDto(myTeam.getName(), myTeam.getPoints()))
                .collect(Collectors.toList()));

        return leagueDto;
    }
}
