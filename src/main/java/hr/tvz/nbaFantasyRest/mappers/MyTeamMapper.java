package hr.tvz.nbaFantasyRest.mappers;

import hr.tvz.nbaFantasyRest.controller.team.dto.MyTeamDto;
import hr.tvz.nbaFantasyRest.model.MyPlayer;
import hr.tvz.nbaFantasyRest.model.MyTeam;

import java.util.stream.Collectors;

public class MyTeamMapper {

    public MyTeamDto entityToDto(MyTeam myTeam){
        MyTeamDto teamDto = new MyTeamDto();
        PlayerMapper playerMapper = new PlayerMapper();

        teamDto.setName(myTeam.getName());
        teamDto.setPoints(myTeam.getPoints());
        teamDto.setOnCourt(myTeam.getPlayers().stream().filter(MyPlayer::getActive).map(playerMapper::myPlayerToDto).collect(Collectors.toList()));
        teamDto.setOnBench(myTeam.getPlayers().stream().filter(myPlayer -> !myPlayer.getActive()).map(playerMapper::myPlayerToDto).collect(Collectors.toList()));

        return teamDto;
    }
}
