package hr.tvz.nbaFantasyRest.mappers;

import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;
import hr.tvz.nbaFantasyRest.model.MyPlayer;

public class PlayerMapper {

    public PlayerDto myPlayerToDto(MyPlayer player) {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setId(player.getPlayer().getId());
        playerDto.setMyPlayerId(player.getId());
        playerDto.setActive(player.getActive());
        playerDto.setName(player.getPlayer().getName());
        playerDto.setSurname(player.getPlayer().getSurname());
        playerDto.setTeamName(player.getPlayer().getTeam().getTeamName());
        playerDto.setStatistics(new PlayerStatisticMapper().entitiyToDto(player.getPlayer().getStatistics()));

        return playerDto;
    }

}
