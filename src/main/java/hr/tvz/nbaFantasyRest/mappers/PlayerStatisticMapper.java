package hr.tvz.nbaFantasyRest.mappers;

import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerStatisticsDto;
import hr.tvz.nbaFantasyRest.model.PlayerStatistics;

public class PlayerStatisticMapper {

    public PlayerStatisticsDto entitiyToDto(PlayerStatistics playerStatistics) {
        PlayerStatisticsDto statisticsDto= new PlayerStatisticsDto();
        statisticsDto.setAssists( playerStatistics.getAssists());
        statisticsDto.setPoints( playerStatistics.getPoints());
        statisticsDto.setRebounds( playerStatistics.getRebounds());
        statisticsDto.setPrice( playerStatistics.getPrice());
        statisticsDto.setHeight( playerStatistics.getHeight());
        statisticsDto.setWeight( playerStatistics.getWeight());
        statisticsDto.setJersey( playerStatistics.getJersey());
        statisticsDto.setYearsPro( playerStatistics.getYearsPro());
        statisticsDto.setDateOfBirth( playerStatistics.getDateOfBirth());

        return  statisticsDto;
    }
}
