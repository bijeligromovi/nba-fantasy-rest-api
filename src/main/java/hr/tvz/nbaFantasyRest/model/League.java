package hr.tvz.nbaFantasyRest.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "my_league")
public class League {

    @Id
    @Column(name = "my_league_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "my_league_sequence")
    @SequenceGenerator(name = "my_league_sequence", sequenceName = "nba_fantasy.my_league_sequence")
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
            name="my_team_my_league",
            joinColumns = {@JoinColumn(name = "my_leageue_id")},
            inverseJoinColumns = {@JoinColumn(name = "my_team_id")}
    )
    private List<MyTeam> teams = new ArrayList<>();
}
