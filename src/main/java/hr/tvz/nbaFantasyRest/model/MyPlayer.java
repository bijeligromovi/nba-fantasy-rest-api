package hr.tvz.nbaFantasyRest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "my_player")
public class MyPlayer {

    public MyPlayer(String playerId, Integer position,  Boolean active, MyTeam myTeam) {
        this.playerId = playerId;
        this.active = active;
        this.position = position;
        this.myTeam = myTeam;
    }

    public MyPlayer() { }

    @Id
    @Column(name = "my_player_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "my_player_sequence")
    @SequenceGenerator(name = "my_player_sequence", sequenceName = "nba_fantasy.my_player_sequence")
    protected Long id;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "player_id", insertable = false, updatable=false)
    private Player player;

    @Column(name = "player_id")
    private String playerId;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "my_team_id")
    private MyTeam myTeam;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "points")
    private Integer points = 0;

    @Column(name = "position")
    private Integer position;


}
