package hr.tvz.nbaFantasyRest.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "my_team")
public class MyTeam {

    @Id
    @Column(name = "my_team_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "points")
    private Integer points = 0;

    @MapsId
    @OneToOne(mappedBy = "myTeam", cascade = CascadeType.MERGE)
    @JoinColumn(name = "my_team_id")
    private ApplicationUser user;

    @OneToMany(mappedBy = "myTeam")
    private List<MyPlayer> players;

    @ManyToMany(mappedBy = "teams")
    private List<League> leagues = new ArrayList<>();
}
