package hr.tvz.nbaFantasyRest.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
@Data
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id")
    protected String id;

    @Column(name = "first_name")
    private String name;

    @Column(name = "last_name")
    private String surname;

    @OneToOne(targetEntity=PlayerStatistics.class, mappedBy="player", cascade = CascadeType.ALL)
    private PlayerStatistics statistics;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;


    public Player() {}

    public Player(String id, String name, String surname){

    }


}
