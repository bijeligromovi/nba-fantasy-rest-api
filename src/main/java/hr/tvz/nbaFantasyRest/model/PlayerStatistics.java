package hr.tvz.nbaFantasyRest.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "player_stats")
public class PlayerStatistics {
    @Id
    @Column(name = "player_stats_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String id;

    @Column(name = "points")
    private Integer points;

    @Column(name = "rebounds")
    private Integer rebounds;

    @Column(name = "assists")
    private Integer assists;

    @Column(name = "price")
    private Integer price;

    @Column(name = "height")
    private Integer height;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "jersey")
    private Integer jersey;

    @Column(name = "years_pro")
    private Integer yearsPro;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;
}
