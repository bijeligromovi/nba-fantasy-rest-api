package hr.tvz.nbaFantasyRest.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "team")
public class Team {
    @Id
    @Column(name = "team_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String id;

    @Column(name = "name")
    public String teamName;

    @Column(name = "abbreviation")
    public String abbreviation;

    @OneToMany(targetEntity=Player.class, mappedBy="team", cascade = CascadeType.ALL)
    public Set<Player> player;




    public Team(){}

    public Team(String id, String teamName){
        this.id = id;
        this.teamName = teamName;
    }
}
