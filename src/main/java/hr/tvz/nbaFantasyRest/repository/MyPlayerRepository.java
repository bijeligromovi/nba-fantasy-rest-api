package hr.tvz.nbaFantasyRest.repository;

import hr.tvz.nbaFantasyRest.model.MyPlayer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyPlayerRepository extends JpaRepository<MyPlayer, Integer> {
}
