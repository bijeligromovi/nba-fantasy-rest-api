package hr.tvz.nbaFantasyRest.repository;

import hr.tvz.nbaFantasyRest.model.Player;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    @Query("SELECT p FROM Player p " +
            "JOIN PlayerStatistics s ON p.id=s.player " +
            " WHERE ((LOWER(p.surname) like LOWER(:name) OR :name IS NULL)" +
            "            OR (LOWER(p.name) like LOWER(:name) OR :name IS NULL))" +
            "   AND (p.team.id = :teamId OR :teamId IS NULL) " +
            "   AND ((s.price >= :low OR :low IS NULL) " +
            "           AND (s.price < :high OR :high IS NULL))")
    List<Player> findByNameTeamAndPriceRange(@Param("name") String name, @Param("teamId") String teamId , @Param("low") Integer low, @Param("high") Integer high);

    @Query("SELECT p FROM Player p " +
            "JOIN PlayerStatistics s ON p.id=s.player " +
            " WHERE ((LOWER(p.surname) like LOWER(:name) OR :name IS NULL)" +
            "            OR (LOWER(p.name) like LOWER(:name) OR :name IS NULL))" +
            "   AND (p.team.id = :teamId OR :teamId IS NULL)")
    List<Player> findByNameTeam(@Param("name") String name, @Param("teamId") String teamId);
}
