package hr.tvz.nbaFantasyRest.repository;

import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<ApplicationUser, Long> {

    boolean existsByUserName(String userName);

    boolean existsByEmail(String email);

    ApplicationUser findByUserName(String userName);
}
