package hr.tvz.nbaFantasyRest.service.league;


import hr.tvz.nbaFantasyRest.controller.league.dto.LeagueDto;

import java.security.Principal;
import java.util.List;

public interface LeagueService {

    List<LeagueDto> getLeagues();
    List<LeagueDto> getMyLeagues(Principal principal);
    void joinLeague(Long id, Principal principal);

    void createLeague(String name);
}
