package hr.tvz.nbaFantasyRest.service.league;

import hr.tvz.nbaFantasyRest.controller.league.dto.LeagueDto;
import hr.tvz.nbaFantasyRest.mappers.LeagueMapper;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.model.League;
import hr.tvz.nbaFantasyRest.model.MyTeam;
import hr.tvz.nbaFantasyRest.repository.LeagueRepository;
import hr.tvz.nbaFantasyRest.repository.MyTeamRepository;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LeagueServiceImpl implements LeagueService {

    @Autowired
    LeagueRepository leagueRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MyTeamRepository myTeamRepository;

    @Override
    public List<LeagueDto> getLeagues() {
        LeagueMapper mapper = new LeagueMapper();
        return leagueRepository.findAll().stream().map(mapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public List<LeagueDto> getMyLeagues(Principal principal) {
        LeagueMapper mapper = new LeagueMapper();
        ApplicationUser user = userRepository.findByUserName(principal.getName());

        return  user.getMyTeam().getLeagues().stream().map(mapper::entityToDto).collect(Collectors.toList());
    }

    @Override
    public void joinLeague(Long id, Principal principal) {
        ApplicationUser user = userRepository.findByUserName(principal.getName());

        MyTeam myTeam = user.getMyTeam();

        League league = leagueRepository.findById(id).get();

        if(myTeam.getLeagues().isEmpty()) {
            myTeam.setLeagues(Collections.singletonList(league));
        } else {
            List<League> leagues = myTeam.getLeagues();
            leagues.add(league);
            myTeam.setLeagues(leagues);
        }

        myTeamRepository.save(myTeam);
    }

    @Override
    public void createLeague(String name) {
        League league = new League();
        league.setName(name);
        leagueRepository.save(league);
    }
}
