package hr.tvz.nbaFantasyRest.service.player;

import hr.tvz.nbaFantasyRest.controller.player.dto.DropdownFiltersValuesDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.FiltersDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;

import java.util.List;

public interface PlayerService {

    List<PlayerDto> getAllPlayers(FiltersDto filtersDto);
    List<DropdownFiltersValuesDto> getDropdownValues();
}
