package hr.tvz.nbaFantasyRest.service.player;

import hr.tvz.nbaFantasyRest.controller.player.dto.DropdownFiltersValuesDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.FiltersDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerStatisticsDto;
import hr.tvz.nbaFantasyRest.model.Player;
import hr.tvz.nbaFantasyRest.model.Team;
import hr.tvz.nbaFantasyRest.repository.PlayerRepository;
import hr.tvz.nbaFantasyRest.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerServiceImpl implements  PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;

    @Override
    public List<PlayerDto> getAllPlayers(FiltersDto filtersDto) {
        List<Player> players = new ArrayList<Player>();
        String[] values = null;
        if(filtersDto.getPlayerName().isEmpty() && filtersDto.getTeamId().isEmpty() && filtersDto.getPrice().isEmpty()){
            players = playerRepository.findAll(PageRequest.of(filtersDto.getPage(), filtersDto.getSize())).getContent();
        }
        else if(!filtersDto.getPrice().isEmpty()){
            values = filtersDto.getPrice().split(";");
            if(values != null && values.length == 2){
                players = playerRepository.findByNameTeamAndPriceRange("%" + filtersDto.getPlayerName ()+ "%", filtersDto.getTeamId(), Integer.parseInt(values[0]), Integer.parseInt(values[1]));

            }else if(values != null && values.length == 1){
                players = playerRepository.findByNameTeamAndPriceRange("%" + filtersDto.getPlayerName ()+ "%", filtersDto.getTeamId(), Integer.parseInt(values[0]), null);
            }
        }else{
            players = playerRepository.findByNameTeamAndPriceRange("%" + filtersDto.getPlayerName ()+ "%", filtersDto.getTeamId(), null, null);
        }

        //TODO kako mapirati nestane liste
//        ModelMapper mapper = new ModelMapper();
//
//        java.lang.reflect.Type targetListType = new TypeToken<List<PlayerDto>>() {}.getType();
//        List<PlayerDto> playerDto = mapper.map(players, targetListType);

        List<PlayerDto> playerDto = new ArrayList<>();
        for(Player player : players){
            PlayerDto newDto = new PlayerDto();
            newDto.setId(player.getId());
            newDto.setName(player.getName());
            newDto.setSurname(player.getSurname());
            if(player.getStatistics() != null){
                PlayerStatisticsDto statisticsDto= new PlayerStatisticsDto();
                statisticsDto.setAssists( player.getStatistics().getAssists());
                statisticsDto.setPoints( player.getStatistics().getPoints());
                statisticsDto.setRebounds( player.getStatistics().getRebounds());
                statisticsDto.setPrice( player.getStatistics().getPrice());
                statisticsDto.setHeight( player.getStatistics().getHeight());
                statisticsDto.setWeight( player.getStatistics().getWeight());
                statisticsDto.setJersey( player.getStatistics().getJersey());
                statisticsDto.setYearsPro( player.getStatistics().getYearsPro());
                statisticsDto.setDateOfBirth( player.getStatistics().getDateOfBirth());
                newDto.setStatistics(statisticsDto);
            }
            playerDto.add(newDto);
        }

        return playerDto;
    }

    @Override
    public List<DropdownFiltersValuesDto> getDropdownValues(){
        List<Team> teams = teamRepository.findAll();
        List<DropdownFiltersValuesDto> ret = new ArrayList<>();

        for (Team team: teams){
            DropdownFiltersValuesDto newFilterItem = new DropdownFiltersValuesDto();
            newFilterItem.setTeamId(team.id);
            newFilterItem.setTeamName(team.teamName);
            ret.add(newFilterItem);
        }

        return ret;
    }
}
