package hr.tvz.nbaFantasyRest.service.security;

import hr.tvz.nbaFantasyRest.controller.security.dto.UserDto;

public interface UserService {

    void registerUser(UserDto user) throws Exception;
}
