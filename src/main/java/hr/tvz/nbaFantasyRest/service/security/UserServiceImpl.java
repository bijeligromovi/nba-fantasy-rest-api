package hr.tvz.nbaFantasyRest.service.security;

import hr.tvz.nbaFantasyRest.controller.security.dto.UserDto;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void registerUser(UserDto userDto) throws Exception {

        if(!userDto.password.equals(userDto.confirmPassword)){
            throw new Exception("Passwords don't match!");
        }
        if(userDto.password.length() < 6 || userDto.password.length() > 100){
            throw new Exception("Password too short. Need at least 6 characters!");
        }
        if(userRepository.existsByUserName(userDto.userName)){
            throw new Exception("Username already exists!");
        }
        if(userRepository.existsByEmail(userDto.email)){
            throw new Exception("Email already exists!");
        }

        ModelMapper mapper = new ModelMapper();
        ApplicationUser userModel = mapper.map(userDto, ApplicationUser.class);
        userModel.setPassword(new BCryptPasswordEncoder().encode(userModel.getPassword()));

        userRepository.save(userModel);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        ApplicationUser user = userRepository.findByUserName(userName);
        if(user == null){
            throw new UsernameNotFoundException(userName);
        }

        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), Collections.emptyList());
    }
}
