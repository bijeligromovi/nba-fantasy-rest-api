package hr.tvz.nbaFantasyRest.service.team;

import hr.tvz.nbaFantasyRest.controller.team.dto.MyTeamDto;
import hr.tvz.nbaFantasyRest.controller.team.dto.SaveTeamDto;

import java.security.Principal;

public interface TeamService {
    void addToTeam(Principal userDetails, SaveTeamDto saveTeamDto) throws Exception;

    MyTeamDto getTeam(Principal principal);

    Boolean teamExsists(Principal principal);
}
