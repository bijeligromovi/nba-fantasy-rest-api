package hr.tvz.nbaFantasyRest.service.team;

import hr.tvz.nbaFantasyRest.controller.team.dto.MyTeamDto;
import hr.tvz.nbaFantasyRest.controller.team.dto.SaveTeamDto;
import hr.tvz.nbaFantasyRest.mappers.MyTeamMapper;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.model.MyPlayer;
import hr.tvz.nbaFantasyRest.model.MyTeam;
import hr.tvz.nbaFantasyRest.repository.MyPlayerRepository;
import hr.tvz.nbaFantasyRest.repository.MyTeamRepository;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Service
@Log4j
public class TeamServiceImpl implements TeamService {

    @Autowired
    MyTeamRepository myTeamRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MyPlayerRepository myPlayerRepository;

    @Override
    @Transactional
    public void addToTeam(Principal principal, SaveTeamDto saveTeamDto) throws Exception {
        if(saveTeamDto.getOnCourt().size() != 5 || saveTeamDto.getSubstitutions().size() != 7){
            throw new Exception("Team size is not correct");
        }

       ApplicationUser user = userRepository.findByUserName(principal.getName());

        MyTeam myTeam = new MyTeam();
        myTeam.setName(saveTeamDto.getName());
        myTeam.setUser(user);
        myTeamRepository.save(myTeam);

        List<MyPlayer> players = new ArrayList<>();

        IntStream.range(0, saveTeamDto.getOnCourt().size()).forEach(i ->
            players.add(new MyPlayer(saveTeamDto.getOnCourt().get(i), i, true, myTeam))

        );
        IntStream.range(0, saveTeamDto.getSubstitutions().size()).forEach(i ->
            players.add(new MyPlayer(saveTeamDto.getSubstitutions().get(i), i, false, myTeam))

        );

        myPlayerRepository.saveAll(players);
    }

    @Override
    public MyTeamDto getTeam(Principal principal) {
        ApplicationUser user = userRepository.findByUserName(principal.getName());
        MyTeam myTeam = user.getMyTeam();
        MyTeamMapper mapper = new MyTeamMapper();

        return mapper.entityToDto(myTeam);
    }

    @Override
    public Boolean teamExsists(Principal principal) {
        ApplicationUser user = userRepository.findByUserName(principal.getName());
        return myTeamRepository.existsById(user.getId());
    }
}
