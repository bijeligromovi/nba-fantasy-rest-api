CREATE SCHEMA IF NOT EXISTS nba_fantasy;

CREATE TABLE IF NOT EXISTS nba_fantasy.user(
  id BIGSERIAL not NULL ,
  name VARCHAR(255),
  last_name VARCHAR(255),
  password VARCHAR(255),
  email VARCHAR(255),
  role VARCHAR(255)
  CONSTRAINT pk_user PRIMARY KEY (id)

) ;