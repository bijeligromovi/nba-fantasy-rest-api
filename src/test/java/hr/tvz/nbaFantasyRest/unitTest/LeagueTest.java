package hr.tvz.nbaFantasyRest.unitTest;

import hr.tvz.nbaFantasyRest.controller.league.dto.LeagueDto;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.model.League;
import hr.tvz.nbaFantasyRest.model.MyTeam;
import hr.tvz.nbaFantasyRest.repository.LeagueRepository;
import hr.tvz.nbaFantasyRest.repository.MyTeamRepository;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import hr.tvz.nbaFantasyRest.service.league.LeagueService;
import hr.tvz.nbaFantasyRest.service.league.LeagueServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LeagueTest {

    @Spy
    @InjectMocks
    LeagueServiceImpl leagueService;

    @Mock
    UserRepository userRepositoryMock;

    @Mock
    MyTeamRepository myTeamRepository;

    @Mock
    LeagueRepository leagueRepository;

    @Mock
    Principal principal;

    List<League> givenLeagues = new ArrayList<>();

    @Before
    public void init() {
        League league1 = new League();
        league1.setName("league1");
        league1.setId(1L);
        League league2 = new League();
        league2.setName("league2");
        league2.setId(2L);
        givenLeagues.add(league1);
        givenLeagues.add(league2);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetLeagues(){
        when(leagueRepository.findAll()).thenReturn(givenLeagues);

        List<LeagueDto> leagueDtos = leagueService.getLeagues();

        assertEquals(leagueDtos.get(0).getName(), "league1");
        assertEquals(leagueDtos.get(1).getName(), "league2");

    }
    @Test
    public void shouldGetUsersLeagues(){
        ApplicationUser givenUser = new ApplicationUser();
        MyTeam givenMyTeam = new MyTeam();
        League givenMyLeague = new League();
        givenMyLeague.setName("league1");
        List<League> givenLeagues = Collections.singletonList(givenMyLeague);
        givenMyTeam.setLeagues(givenLeagues);
        givenUser.setMyTeam(givenMyTeam);

        when(leagueRepository.findAll()).thenReturn(givenLeagues);
        when(principal.getName()).thenReturn("name");
        when(userRepositoryMock.findByUserName(any())).thenReturn(givenUser);

        List<LeagueDto> leagueDtos = leagueService.getMyLeagues(principal);

        assertEquals(leagueDtos.get(0).getName(), "league1");
    }
    @Test
    public void shouldCreateLeague(){
        League givenLeague = new League();
        givenLeague.setName("league");

        ArgumentCaptor<League> captor= ArgumentCaptor.forClass(League.class);
        leagueService.createLeague(givenLeague.getName());
        verify(leagueRepository).save(captor.capture());
        assertEquals(captor.getValue().getName(), "league");

    }
}
