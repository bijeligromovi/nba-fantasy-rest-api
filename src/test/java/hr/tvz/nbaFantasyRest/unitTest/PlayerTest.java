package hr.tvz.nbaFantasyRest.unitTest;

import hr.tvz.nbaFantasyRest.controller.player.dto.DropdownFiltersValuesDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.FiltersDto;
import hr.tvz.nbaFantasyRest.controller.player.dto.PlayerDto;
import hr.tvz.nbaFantasyRest.model.League;
import hr.tvz.nbaFantasyRest.model.Player;
import hr.tvz.nbaFantasyRest.model.Team;
import hr.tvz.nbaFantasyRest.repository.PlayerRepository;
import hr.tvz.nbaFantasyRest.repository.TeamRepository;
import hr.tvz.nbaFantasyRest.service.player.PlayerService;
import hr.tvz.nbaFantasyRest.service.player.PlayerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class PlayerTest {


    @Spy
    @InjectMocks
    private PlayerServiceImpl playerService;

    @Mock
    private PlayerRepository playerRepository;

    @Mock
    private TeamRepository teamRepository;

    // mockane liste
    List<Player> givenPlayers = new ArrayList<>();
    List<Team> givenTeams = new ArrayList<>();



    @Before
    public void init() {

        // mockanje playera
        Player player1 = new Player("1L","Michael", "Jordan");
        Player player2 = new Player("2L","Aries", "Aaron");
        Player player3 = new Player("3L","Kelsy", "Phelps");
        givenPlayers.add(player1);
        givenPlayers.add(player2);
        givenPlayers.add(player3);

        // mockanje teamova
        Team team1 = new Team("1","Bulls");
        Team team2 = new Team("2","Raptors");
        Team team3 = new Team("3","Cavs");
        givenTeams.add(team1);
        givenTeams.add(team2);
        givenTeams.add(team3);

        // pokretanje svih mock anotacija prije testova
        // omogucava koristenje @Mock i @Spy
        MockitoAnnotations.initMocks(this);
    }



    @Test
    public void shouldGetDropdownValues(){

        // vrati mockani tim
        when(teamRepository.findAll()).thenReturn(givenTeams);

        // pozovi metodu iz servisa
        List<DropdownFiltersValuesDto> dropdownValues = playerService.getDropdownValues();

        // provjeri  odgovaraju li vrijednosti
        assertEquals(dropdownValues.get(0).getTeamName(), "Bulls");
        assertEquals(dropdownValues.get(1).getTeamName(), "Raptors");
        assertEquals(dropdownValues.get(2).getTeamName(), "Cavs");

    }




//    @Test
//    public void shouldGetAllPlayers(){
//
//        FiltersDto filtersDto1 = new FiltersDto("","","",1,3);
//
//        when(playerRepository.findAll()).thenReturn(givenPlayers);
//
//        List<PlayerDto> playerDtos = playerService.getAllPlayers(filtersDto1);
//
//
//        assertEquals(playerDtos.get(0).getName(), "Michael");
//        assertEquals(playerDtos.get(1).getName(), "Aries");
//        assertEquals(playerDtos.get(2).getName(), "Kelsy");
//
//    }




}
