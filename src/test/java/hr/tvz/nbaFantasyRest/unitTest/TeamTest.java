package hr.tvz.nbaFantasyRest.unitTest;


import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.repository.MyPlayerRepository;
import hr.tvz.nbaFantasyRest.repository.MyTeamRepository;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import hr.tvz.nbaFantasyRest.service.team.TeamService;
import hr.tvz.nbaFantasyRest.service.team.TeamServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class TeamTest {

    //servis u kojeg želimo injektirati mockove
    @Spy
    @InjectMocks
    private TeamServiceImpl teamService;

    // @mock umjesto autowireanog repozitorija, servisa, itd.. stavi naš neki objekt nad kojim možemo promatrati promjene
    @Mock
    private Principal principal;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private MyTeamRepository myTeamRepository;

    @Mock
    private MyPlayerRepository myPlayerRepositoryMock;

    //prije pocetka testva treba inicijalizirati movkove, ovo se obicno stavi u neku baseTest klasu koju onda svi ostali testovi nasljeđuju
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

    }
    @Test
    public void teamShouldExist() {

        //kreiram usera kojeg ce se vratiti kada se pozove findByUsername
        ApplicationUser givenUser = new ApplicationUser();
        givenUser.setId(1L);

        //kada se pozove principal.getName() mockam da vrati "name"
        when(principal.getName()).thenReturn("name");

        //mock kada se pozove findByUsername sa bilo kojim parametrom u ovom testu vraca usera kojeg smo kreirali ranije
        when(userRepositoryMock.findByUserName("name")).thenReturn(givenUser);

        //mock kada se pozove existById na myTeamRepositoryu sa danim id-jem od usera vrati true
        when(myTeamRepository.existsById(eq(givenUser.getId()))).thenReturn(true);

        // pozivamo servis i spremamo rezultat
        boolean result = teamService.teamExsists(principal);

        //provjera da se userRepository.findByUsername pozvao sa prametrom "name"
        verify(userRepositoryMock, timeout(1000)).findByUserName(eq("name"));

        //provjera da se myTeamRepository.existsById pozvao sa id-jem od danog usera
        verify(myTeamRepository, timeout(1000)).existsById(givenUser.getId());

        //provjera dali smo dobili true kao rezultat
        assertEquals(result, true);

    }

}
