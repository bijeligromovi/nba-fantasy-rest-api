package hr.tvz.nbaFantasyRest.unitTest;

import hr.tvz.nbaFantasyRest.controller.user.UserController;
import hr.tvz.nbaFantasyRest.controller.user.dto.UserDto;
import hr.tvz.nbaFantasyRest.model.ApplicationUser;
import hr.tvz.nbaFantasyRest.model.MyTeam;
import hr.tvz.nbaFantasyRest.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.Principal;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private Principal principal;

    @Mock
    private UserRepository userRepositoryMock;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUserTest() {

        ApplicationUser fakeUser = new ApplicationUser();
        UserDto fakeUserDto = new UserDto();
        MyTeam team = new MyTeam();

        team.setName("Josko Team");

        fakeUser.setFirstName("Josko");
        fakeUser.setLastName("Jelicic");
        fakeUser.setMyTeam(team);

        when(principal.getName()).thenReturn("userJosko");

        when(userRepositoryMock.findByUserName(any())).thenReturn(fakeUser);

        // pozivamo servis i spremamo odgovor u objekt
        fakeUserDto = userController.getUser(principal);

        verify(userRepositoryMock, timeout(1000)).findByUserName(eq("userJosko"));

        //provjere vracamo li ok userDto objekt
        assertThat(fakeUserDto.getFirstname(), is("Josko"));
        assertThat(fakeUserDto.getLastname(), is("Jelicic"));
        assertThat(fakeUserDto.getTeamname(), is("Josko Team"));
    }

}
